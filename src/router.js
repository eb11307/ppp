import Vue from 'vue';
import Router from 'vue-router';
Vue.use(Router);
// test to see if this change will show up on erics-testing branch

const routerOptions = [
  { path: "/", component: "Home", meta: { requiresAuth: false } },
  { path: "/contact_info", component: "ContactInfo", meta: { requiresAuth: false } },
  { path: "/consents", component: "Consents", meta: { requiresAuth: false } },
  { path: "/faqs", component: "FAQs", meta: { requiresAuth: false } },
  { path: "/itinerary", component: "Itinerary", meta: { requiresAuth: false} },
  { path: "*", redirect:"/" }
];

const routes = routerOptions.map(route => {
  return {
    ...route,
    component: () => import(`./views/${route.component}.vue`)
  };
});

export default new Router({
    mode: 'history',
    base: process.env.BASE_URL,
    routes: routes
});