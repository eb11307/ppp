import Vue from 'vue';
import App from './App.vue';
import vuetify from './plugins/vuetify';
import router from './router.js';
import { ValidationProvider, extend } from 'vee-validate';
import { required } from 'vee-validate/dist/rules';

extend('required', {
  ...required,
  message: 'The {_field_} field is required'
});

Vue.config.productionTip = false;

new Vue({
  vuetify,
  router,
  ValidationProvider,
  render: h => h(App)
}).$mount('#app');
