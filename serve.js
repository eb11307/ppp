const http = require('http');
const fs = require('fs');
const httpPort = 3001;

http.createServer((req, res) => {

  fs.readFile('/opt/ppmi-participant-portal/dist/index.html', 'utf-8', (err, content) => {
    if(err) {
      console.log("Error opening index.htm");
    }

    res.writeHead(200, {
      'Content-Type': 'text/html; charset=utf-8'
    })

    res.end(content);

  })

}).listen(httpPort, () => {
  console.log('Server listening on port %s', httpPort);
})